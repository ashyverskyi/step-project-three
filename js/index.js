"use strict";

const DATA = [
    {
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m1.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "8 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f1.png",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m2.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        "first name": "Тетяна",
        "last name": "Мороз",
        photo: "./img/trainers/trainer-f2.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
    },
    {
        "first name": "Сергій",
        "last name": "Коваленко",
        photo: "./img/trainers/trainer-m3.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
    },
    {
        "first name": "Олена",
        "last name": "Лисенко",
        photo: "./img/trainers/trainer-f3.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
    },
    {
        "first name": "Андрій",
        "last name": "Волков",
        photo: "./img/trainers/trainer-m4.jpg",
        specialization: "Бійцівський клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
    },
    {
        "first name": "Наталія",
        "last name": "Романенко",
        photo: "./img/trainers/trainer-f4.jpg",
        specialization: "Дитячий клуб",
        category: "спеціаліст",
        experience: "3 роки",
        description:
            "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
    },
    {
        "first name": "Віталій",
        "last name": "Козлов",
        photo: "./img/trainers/trainer-m5.jpg",
        specialization: "Тренажерний зал",
        category: "майстер",
        experience: "10 років",
        description:
            "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
    },
    {
        "first name": "Юлія",
        "last name": "Кравченко",
        photo: "./img/trainers/trainer-f5.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
    },
    {
        "first name": "Олег",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-m6.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "12 років",
        description:
            "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
    },
    {
        "first name": "Лідія",
        "last name": "Попова",
        photo: "./img/trainers/trainer-f6.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
    },
    {
        "first name": "Роман",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m7.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
    },
    {
        "first name": "Анастасія",
        "last name": "Гончарова",
        photo: "./img/trainers/trainer-f7.jpg",
        specialization: "Басейн",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
    },
    {
        "first name": "Валентин",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-m8.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
    },
    {
        "first name": "Лариса",
        "last name": "Петренко",
        photo: "./img/trainers/trainer-f8.jpg",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "7 років",
        description:
            "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
    },
    {
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m9.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "11 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f9.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m10.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        "first name": "Наталія",
        "last name": "Бондаренко",
        photo: "./img/trainers/trainer-f10.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "8 років",
        description:
            "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
    },
    {
        "first name": "Андрій",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m11.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
    },
    {
        "first name": "Софія",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-f11.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "6 років",
        description:
            "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
    },
    {
        "first name": "Дмитро",
        "last name": "Ковальчук",
        photo: "./img/trainers/trainer-m12.png",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
    },
    {
        "first name": "Олена",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-f12.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "5 років",
        description:
            "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
    },
];



// TASK 1 CARD OPENING

document.addEventListener("DOMContentLoaded", function () {
    const container = document.querySelector('.trainers-cards__container');
    const template = document.getElementById('trainer-card');

    DATA.forEach(trainer => {
        const clone = document.importNode(template.content, true);

        clone.querySelector('.trainer__img').setAttribute('src', trainer.photo);
        clone.querySelector('.trainer__name').textContent = `${trainer["first name"]} ${trainer["last name"]}`;

        container.appendChild(clone);
    });
});


// TASK 2 SORT & FILTER CONTAINERS DISPLAY

document.querySelector('.sidebar').removeAttribute('hidden');
document.querySelector('.sorting').removeAttribute('hidden');


// TASK 3 TRAINER MODAL WINDOW

document.addEventListener('click', function (event) {
    const target = event.target;

    if (target.classList.contains('trainer__show-more')) {
        const trainerCard = target.closest('.trainer');
        const modalTemplate = document.getElementById('modal-template');
        const modalClone = document.importNode(modalTemplate.content, true);

        modalClone.querySelector('.modal__img').setAttribute('src', trainerCard.querySelector('.trainer__img').getAttribute('src'));
        modalClone.querySelector('.modal__name').textContent = trainerCard.querySelector('.trainer__name').textContent;

        const trainer = DATA.find(item => item.photo === trainerCard.querySelector('.trainer__img').getAttribute('src'));
        modalClone.querySelector('.modal__point--category').textContent = `Категорія: ${trainer.category}`;
        modalClone.querySelector('.modal__point--experience').textContent = `Досвід: ${trainer.experience}`;
        modalClone.querySelector('.modal__point--specialization').textContent = `Напрям тренера: ${trainer.specialization}`;
        modalClone.querySelector('.modal__text').textContent = trainer.description;

        document.body.appendChild(modalClone);

        document.body.style.overflow = 'hidden';
    }
});

function closeModal() {
    const modal = document.querySelector('.modal');
    modal.remove();

    document.body.style.overflow = '';
}


// TASK 4 SORTING

// ACTIVE BUTTON
function activateSortingButton() {
    const sortingButtons = document.querySelectorAll('.sorting__btn');

    sortingButtons.forEach(button => {
        button.addEventListener('click', function() {
            sortingButtons.forEach(btn => btn.classList.remove('sorting__btn--active'));
            this.classList.add('sorting__btn--active');
        });
    });
}

activateSortingButton();

// BY DEFAULT
const originalData = [...DATA];

const sortByDefaultButton = document.querySelector('.sorting__btn:nth-of-type(1)');
const container = document.querySelector('.trainers-cards__container');
const template = document.getElementById('trainer-card');

sortByDefaultButton.addEventListener('click', function() {
    container.innerHTML = '';

    originalData.forEach(trainer => {
        const clone = document.importNode(template.content, true);

        clone.querySelector('.trainer__img').setAttribute('src', trainer.photo);
        clone.querySelector('.trainer__name').textContent = `${trainer["first name"]} ${trainer["last name"]}`;

        container.appendChild(clone);
    });
});

// BY LASTNAME
let isAscendingLastName = true;

const sortByLastNameButton = document.querySelector('.sorting__btn:nth-of-type(2)');

sortByLastNameButton.addEventListener('click', function() {
    sortByLastName();
});

function sortByLastName() {
    isAscendingLastName = !isAscendingLastName;

    DATA.sort((a, b) => {
        const lastNameA = a["last name"].toLowerCase();
        const lastNameB = b["last name"].toLowerCase();

        if (isAscendingLastName) {
            return lastNameA.localeCompare(lastNameB, 'uk');
        } else {
            return lastNameB.localeCompare(lastNameA, 'uk');
        }
    });

    renderTrainersCards();
}

// BY EXPERIENCE
let isAscending = true;

const sortByExperienceButton = document.querySelector('.sorting__btn:nth-of-type(3)');

sortByExperienceButton.addEventListener('click', function() {
    sortByExperience();
});

function sortByExperience() {
    isAscending = !isAscending;

    DATA.sort((a, b) => {
        const experienceA = parseInt(a.experience);
        const experienceB = parseInt(b.experience);

        if (isAscending) {
            return experienceA - experienceB;
        } else {
            return experienceB - experienceA;
        }
    });

    renderTrainersCards();
}

// SORTED CARD DISPLAY FUNCTION
function renderTrainersCards() {
    const container = document.querySelector('.trainers-cards__container');
    container.innerHTML = '';

    DATA.forEach(trainer => {
        const template = document.getElementById('trainer-card');
        const clone = document.importNode(template.content, true);

        clone.querySelector('.trainer__img').setAttribute('src', trainer.photo);
        clone.querySelector('.trainer__name').textContent = `${trainer["first name"]} ${trainer["last name"]}`;

        container.appendChild(clone);
    });
}


// TASK 5 FILTERING

// FILTERED CARD DISPLAY FUNCTION
function showTrainers(filteredTrainers) {
    const container = document.querySelector('.trainers-cards__container');
    container.innerHTML = '';

    filteredTrainers.forEach(trainer => {
        const template = document.getElementById('trainer-card');
        const clone = document.importNode(template.content, true);

        clone.querySelector('.trainer__img').setAttribute('src', trainer.photo);
        clone.querySelector('.trainer__name').textContent = `${trainer["first name"]} ${trainer["last name"]}`;

        container.appendChild(clone);
    });
}

// FILTERING FUNCTION
function filterTrainersByCriteria(trainer) {
    const instructorChecked = document.getElementById('instructor').checked;
    const masterChecked = document.getElementById('master').checked;
    const specialistChecked = document.getElementById('specialist').checked;
    const gymChecked = document.getElementById('gym').checked;
    const fightClubChecked = document.getElementById('fight-club').checked;
    const kidsClubChecked = document.getElementById('kids-club').checked;
    const swimmingPoolChecked = document.getElementById('swimming-pool').checked;
    const allCategoryChecked = document.getElementById('all-category').checked;
    const allDirectionChecked = document.getElementById('all-direction').checked;

    return (
        (allCategoryChecked || instructorChecked && trainer.category === 'інструктор') ||
        (allCategoryChecked || masterChecked && trainer.category === 'майстер') ||
        (allCategoryChecked || specialistChecked && trainer.category === 'спеціаліст')
    ) && (
        (allDirectionChecked || gymChecked && trainer.specialization === 'Тренажерний зал') ||
        (allDirectionChecked || fightClubChecked && trainer.specialization === 'Бійцівський клуб') ||
        (allDirectionChecked || kidsClubChecked && trainer.specialization === 'Дитячий клуб') ||
        (allDirectionChecked || swimmingPoolChecked && trainer.specialization === 'Басейн')
    );
}

const showButton = document.querySelector('.filters__submit');
showButton.addEventListener('click', function(event) {
    event.preventDefault();

    const filteredTrainers = DATA.filter(filterTrainersByCriteria);
    showTrainers(filteredTrainers);
});














































































































